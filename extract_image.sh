#!/bin/bash
while read line; do
	shared_file_url=$(echo ${line} | cut -c10-)
	shared_file_url=${shared_file_url%?}
	shared_file=$(wget -qO- "$shared_file_url" | grep ActualMedia | awk '{print $2}' | grep -o steamuserimages.*)
	shared_file=${shared_file%?}
	echo $shared_file
done < /dev/stdin
