#!/bin/bash
i=16 # larger than the last page
user="smileydan2"
while (( --i >= 1 )); do
	grid=$(curl -s "https://steamcommunity.com/id/$user/screenshots/?p=$i&sort=newestfirst&browsefilter=myfiles&view=grid&privacy=14" 2>/dev/null)
	grep -o "\"https://steamcommunity.com/sharedfiles/filedetails/.*\"" <<< $grid
done
