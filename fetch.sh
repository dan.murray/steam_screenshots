#!/bin/bash

c=0
while read line; do
	curl --output screenshot_$(printf "%04d" $c).jpg $line
	let c=c+1
done < /dev/stdin
